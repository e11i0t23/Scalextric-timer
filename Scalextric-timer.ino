#include "definitions.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Ticker.h>

Ticker track1_ticker;
Ticker track2_ticker;
// Update these with values suitable for your network.

const char* ssid = wifiID;
const char* password = PWD;
const char* mqtt_server = "192.168.0.27";


const int green = 14;
const int red1 = 2;
const int red2 = 0;
const int red3 = 12;

const int t1_pin = 5;
const int t2_pin = 16;
int laps = 1;
int t1_laps = 0;
float t2_laps = 0;
float t1_time = 0;
float t2_time = 0;
float t1_fastest = 1000;
float t2_fastest = 1000;
int t1_last = 0;
int t2_last = 0;

long randomNumber;

String intarr[10] = {"0","1","2","3","4","5","6","7","8","9"};


WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;


bool race = false;
bool allnum = true;
bool isnum = false;
bool lights = false;


void setup() {
  pinMode(t1_pin, INPUT);
  pinMode(t2_pin, INPUT);

  pinMode(green, OUTPUT);
  pinMode(red1, OUTPUT);
  pinMode(red2, OUTPUT);
  pinMode(red3, OUTPUT);
  track1_ticker.attach(0.01, track1_timer);
  track2_ticker.attach(0.01, track2_timer);

  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  randomSeed(42);
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  String text;
  for (int i = 0; i < length; i++) {
    isnum = false;
    Serial.print((char)payload[i]);
    text += (char)payload[i];
    for (int x = 0; x < 10; x++){
      if ((String)payload[i] == intarr[x]){
        isnum = true;
        race = false;
      }
    }
    if (isnum != true){
      allnum = false;
    }
  }
  if (allnum = true){
    client.publish("PlayerOneLaps", "Race Starting");
    client.publish("PlayerTwoLaps", "Race Starting");
    laps = text.toInt();
    t1_laps = 0;
    t2_laps = 0;
    t1_last = 0;
    t2_last = 0;
    t1_fastest = 1000;
    t2_fastest = 1000;
    lights = true;
    race = true;
  }else if (text == "end"){
    race = false;
  }
  Serial.println();
  Serial.println(text);



}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  if (lights == true){
    randomNumber = random(1,5)*1000;
    Serial.print("Delay of: ");
    Serial.println(randomNumber);
    digitalWrite(red1, HIGH); 
    delay(1000); 
    digitalWrite(red1, LOW);              
    digitalWrite(red2, HIGH);
    delay(1000);
    digitalWrite(red2, LOW);
    digitalWrite(red3, HIGH);
    delay(1000);
    digitalWrite(red3, LOW);
    digitalWrite(green, HIGH);
    delay(randomNumber);
    digitalWrite(green, LOW);
    client.publish("PlayerOneLaps", "Go");
    client.publish("PlayerTwoLaps", "Go");
    t1_time = 0;
    t2_time = 0;
    delay(500);
    lights = false;     
  }

  if (race == true){
    //Serial.println("We are racing");
    if (digitalRead(t1_pin) == HIGH){
    Serial.println("Player one completed a lap");
    snprintf (msg, 75, "Laps Remaining #%ld", laps-t1_laps);
    client.publish("PlayerOneLaps", msg);
    t1_laps = t1_laps + 1;
    if (t1_laps == laps){
        Serial.println("Player one won!");
        delay(100);
        snprintf (msg, 75, "You won with fastest lap of %f", t1_fastest);
        client.publish("PlayerOneLaps", msg);
        client.publish("PlayerTwoLaps", "You Lost Maybe Next time");
        race = false;
    }
    delay(50);
    }if (digitalRead(t2_pin) == HIGH){
    Serial.println("Player one completed a lap");
    snprintf (msg, 75, "Laps Remaining #%ld", laps-t2_laps);
    client.publish("PlayerTwoLaps", msg);
    t2_laps = t2_laps + 1;
    if (t2_laps == laps){
        Serial.println("Player two won!");
        delay(100);
        snprintf (msg, 75, "You won with fastest lap of %f", t2_fastest);
        client.publish("PlayerOneLaps", "You Lost Maybe Next time");
        client.publish("PlayerTwoLaps", msg);
        race = false;
    }
    delay(50);
    }
  }
}

void track1_timer(){
  if (t1_last!=t1_laps){
    t1_last = t1_laps;
    Serial.print("Player 1 completed lap in ");
    Serial.print(t1_time);
    Serial.println(" second(s)");
    if (t1_time<t1_fastest){
      t1_fastest = t1_time;
    }
    t1_time = 0;
  }else{
    t1_time += 0.01;
  }
}

void track2_timer(){
   if (t2_last!=t2_laps){
    t2_last = t2_laps;
    Serial.print("Player 2 completed lap in ");
    Serial.print(t2_time);
    Serial.println(" second(s)");
    if (t2_time<t2_fastest){
      t2_fastest = t2_time;
    }
    t2_time = 0;
  }else{
    t2_time += 0.01;
  }
}
